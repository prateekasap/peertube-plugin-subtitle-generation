const multer = require('multer'); 
const path = require('path');
const dir = '/data/captions';
const AWS = require('aws-sdk');
const nodeFetch = require('node-fetch');
require('dotenv').config({path: __dirname + '/.env'});
// store uploaded .vtt file in the captions folder of peertubue
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, dir)
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + path.extname(file.originalname))
  }
})

// set multer upload details and file limit
const upload = multer({ storage: storage, limits : {fileSize : 2000000} });

async function register ({
  registerHook,
  peertubeHelpers,
  registerSetting,
  settingsManager,
  getRouter
}) {


  console.log('access key is',process.env.AWS_ACCESS_KEY, process.env.AWS_REGION)
  const router = getRouter()

  // query to insert data to videoCaption table
  const addSubToDb = async (videoId, fileName) => {

    // get videoId from uuid 
    let [vidId] = await peertubeHelpers.database.query(
      `SELECT id from "video" where uuid = $videoId`,
      {
        type: "SELECT",
        bind: {videoId},
      }
    )
    console.log('videoId is', vidId);

    let queryResult = await peertubeHelpers.database.query(
      `INSERT INTO "videoCaption" ("language", "filename", "fileUrl", "videoId", "createdAt", "updatedAt") VALUES ('ja', $fileName, null, $videoId, now(), now())`,
      {
        type: "INSERT",
        bind: { fileName, videoId: vidId.id},
      }
    );

    return queryResult
  }

  router.post('/subtitle', upload.single('file'),  async(req,res) => {
    try {
      const data = req.file
      const {VideoID} = req.body  

      if (data) {
      // update vidoecaption table
      const result =  await addSubToDb(VideoID, req.file.filename)
        res.send({
          data, 
          filename: req.file.filename,
          VideoID, 
          result
        });
      }
    } catch (error) {
      console.log('error is',error)
      throw error
    }
   
  
  })


  router.post('/auto-gen', async(req,res) => {
      const {vidId} = req.body;


      const apiKey = await settingsManager.getSettings(['ACCESS-KEY', 'SECRET-KEY', 'REGION']);

      //check if the needed keys are present and not empty
      const neededKeys = ['ACCESS-KEY', 'SECRET-KEY', 'REGION'];
      console.log('apiKey', apiKey);
  
      if (!apiKey) return res.status(400).json({ success: false, message: 'AWS KEYS NOT SET for plugin'})
      if (!neededKeys.every(key =>  Object.keys(apiKey).includes(key))) return res.status(400).json({ success: false, message: 'AWS KEYS NOT SET for plugin'});

      // https://terakoya-backstage.com/api/v1/videos/${vidId}

      //get video download url
      const videoInfo = await fetch(`https://terakoya-backstage.com/api/v1/videos/${vidId}`);
      const finalData = await videoInfo.json()


      const downUrl = finalData.streamingPlaylists[0].files[0].fileDownloadUrl
      const lambdaParams = {
        videoId: vidId,
        donwloadUrl: downUrl
      }

   
      const params = {
        FunctionName: 'lambda-subtitle-oss',
        Payload: JSON.stringify(lambdaParams)
      };


        AWS.config.update({accessKeyId: apiKey['ACCESS-KEY'], secretAccessKey: apiKey['SECRET-KEY'], region: apiKey['REGION']});

        const lambda = new AWS.Lambda();
      try {
        lambda.invoke(params,  function(err, data) {
          if (err) console.log(err, err.stack); // an error occurred
          else  {
            console.log('datais', data)
            //check if s3 bucket url is returned
            // if (data.Payload) {
            //   //on successful uri response
            //   const splitSting =  data.Payload.split('"')[1]
              
            //   // https://vtt-oss.terakoya-backstage.com/whipser
            //   //call python endpoint
            //   nodeFetch('http://127.0.0.1:8000/whisper', {
            //     method: 'POST',
            //     headers: { 'Content-Type': 'application/json' },
            //     body: JSON.stringify({audio: splitSting})
            //   }).then(res => {
            //     return res
            //   }).then(res => {
            //     console.log('return data', res)
            //   }).catch(err => {
            //     console.log('error is', err)
            //   })
            // } else {
            //   console.log('nodata')
            // }
            
          } 
        });
      
      } catch (error) {
        console.log('error is', error)
        throw error
      }

      res.status(200).json({
        message: 'success',
        data: lambdaParams
      });

  });
  



  registerSetting({
    name: 'ACCESS-KEY',
    label: 'ACCESS_KEY',
    type: 'input',

    descriptionHTML: 'Add you ACCESS KEY HERE...',

    default: 'JKlM1234ABC_Z',

    // If the setting is not private, anyone can view its value (client code included)
    // If the setting is private, only server-side hooks can access it
    private: true
  })


  registerSetting({
    name: 'SECRET-KEY',
    label: 'SECRET_KEY',
    type: 'input',

    descriptionHTML: 'Add you SECRET KEY HERE...',

    default: 'JKlM1234ABC_Z',

    // If the setting is not private, anyone can view its value (client code included)
    // If the setting is private, only server-side hooks can access it
    private: true
  })

  registerSetting({
    name: 'REGION',
    label: 'REGION',
    type: 'input',

    descriptionHTML: 'Add you AWS REGION HERE...',

    default: 'ap-south-3',

    // If the setting is not private, anyone can view its value (client code included)
    // If the setting is private, only server-side hooks can access it
    private: true
  })

  settingsManager.onSettingsChange(settings => {
   
  })

}

async function unregister () {
  return
}

module.exports = {
  register,
  unregister
}
