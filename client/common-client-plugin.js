function register ({ registerHook, peertubeHelpers }) {
    registerHook({
      target: 'action:video-edit.init',
      handler:  () => {
      //get uuid from url
        const data =window.location.href;

        const videoUuid = data.split('/').pop()
  
        const tabElem = document.querySelector('div.nav-tabs.nav')

        tabElem.onclick = function () {
          const checkActiveClass = tabElem.children[1].classList.contains('active');
          console.log('check active class', checkActiveClass);
          if (checkActiveClass) {

            //create button for auto subtitle gen
            let buttonEl = document.createElement("a");
            buttonEl.setAttribute('class', 'subtitle-btn');
	          let buttonTextEl = document.createElement("span");
	          buttonTextEl.innerText = "Auto-generate Subtitle";
	          buttonEl.appendChild(buttonTextEl);
         
            const editDiv = document.querySelector('div.captions-header');
          
            //append button to the dom if it doesnot exist already
            const checkButtonExists = document.querySelector('a.subtitle-btn')
            if (!checkButtonExists) {
              editDiv.appendChild(buttonEl);
            }


            buttonEl.addEventListener('click',  async function(e) {
              e.preventDefault()
              const vidInfo = {
                vidId: videoUuid
              }

              // https://terakoya-backstage.com/plugins/subtitle-generation/router/auto-gen
              const finalData = await fetch('https://terakoya-backstage.com/plugins/subtitle-generation/router/auto-gen', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(vidInfo)
              });

              const isJson = await finalData.json();

              return isJson
            });

          }
        }

      }
    })
}

export {
  register
}
